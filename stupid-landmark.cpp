#include <onnxruntime/core/providers/cpu/cpu_provider_factory.h>
#include <onnxruntime/core/session/onnxruntime_cxx_api.h>
#include <opencv2/opencv.hpp>
#include <exception>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <cmath>
#include <sstream>

Ort::Env &getEnv(void)
{
	static Ort::Env env(OrtLoggingLevel::ORT_LOGGING_LEVEL_WARNING,
											"mosesNamespace");

	return env;
}

void drawBB(float cX, float cY, float w, float h, cv::Mat &img)
{
	// cX*=224.0;
	// cY*=224.0;
	// w*=224.0;
	// h*=224.0;
	cv::circle(img, {(int)cX, (int)cY}, (int)(w / 10), {0, 0, 255});

	cv::Rect rect((int)cX - w / 2, (int)cY - h / 2, (int)w, (int)h);
	cv::rectangle(img, rect, {0, 0, 255});
}

int main(int argc, char **argv)
{
	std::cout << "Usage: moses-onnx-example <model-file> <image-file>\n";
	GraphOptimizationLevel onnx_optim = GraphOptimizationLevel::ORT_ENABLE_ALL;
	Ort::SessionOptions sessionOptions;
	sessionOptions.SetGraphOptimizationLevel(onnx_optim);
	char *modelLocation = "hand_landmark.onnx";
	Ort::Session *session;
	Ort::Session session1(getEnv(), modelLocation, sessionOptions);
	session = &session1;
	size_t numInputNodes = session->GetInputCount();
	std::cout << "Number of Input Nodes: " << numInputNodes << std::endl;

	Ort::TypeInfo inputTypeInfo = session->GetInputTypeInfo(0);
	// Ort::TensorTypeAndShapeInfo inputTASI = inputTypeInfo.GetTensorTypeAndShapeInfo();
	// int64_t cool[10] = {0};
	// inputTASI.GetDimensions(cool, 10);
	// for (size_t i = 0; i < 10; i++) {
	// 	printf("dim %d\n", cool[i]);
	// }

	std::vector<int64_t> inputDims = inputTypeInfo.GetTensorTypeAndShapeInfo().GetShape();
	size_t num_dims = inputDims.size();
	assert(num_dims == 4); // That's what the hand tracking model wants right now; if it's not four we have the wrong guy
	std::cout << "number of dimensions is " << inputDims.size() << "\n";
	for (size_t i = 0; i < num_dims; i++)
	{
		std::cout << "dimension " << i << " is " << inputDims[i] << "\n";
	}

	std::vector<const char *> outputNames;
	Ort::AllocatorWithDefaultOptions allocator;
	char *inputName = session->GetInputName(0, allocator);
	printf("Input name: %s\n", inputName);

	for (size_t i = 0; i < session->GetOutputCount(); ++i)
	{
		auto output_name = session->GetOutputName(i, allocator);
		std::cout << "output " << i << " is " << output_name << std::endl;
		// outputNames.push_back (output_name);
		auto type_info = session->GetOutputTypeInfo(i);
		auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
		std::vector<int64_t> outputDims = tensor_info.GetShape();
		for (size_t i = 0; i < outputDims.size(); i++)
		{
			std::cout << "   dimension " << i << " is " << outputDims[i] << "\n";
		}
		// std::cout << "shape is " << tensor_info.GetShape();
	}
	outputNames.push_back("Identity");
	outputNames.push_back("Identity_1");
	outputNames.push_back("Identity_2");

	Ort::MemoryInfo memoryInfo = Ort::MemoryInfo::CreateCpu(
			OrtAllocatorType::OrtArenaAllocator, OrtMemType::OrtMemTypeDefault);

	const size_t inputTensorSize = 224 * 224 * 3; // mostly from Netron and other testing
	std::vector<cv::Mat> planes;

	// Load image

	// cv::Mat img = cv::imread("righthand.png");
	// cv::VideoCapture capture;
	// capture.open(0);
	// printf("cool %d\n", capture.isOpened());
	// ASSUMES the camera is 1280x960

	// cv::Mat raw_input;
	// while (true)
	// {

	// raw_input = cv::imread("trylandmark.png");

		// std::cout << "rawcontinuous? " << raw_input.isContinuous() << "\n";

		cv::Mat img =  cv::imread("try-landmark.png");


		cv::resize(img, img, {224, 224});



		std::cout << "shape "<< "continuous? " << img.isContinuous() << "\n";
		assert(img.isContinuous());

		// Make image planar instead of interleaved
		cv::split(img, planes);
		cv::Mat red = planes[2];
		cv::Mat green = planes[1];
		cv::Mat blue = planes[0];
		uint8_t combined_planes[224 * 224 * 3] = {0};
		memcpy(combined_planes, red.data, 224 * 224);
		memcpy(combined_planes + (224 * 224), green.data, 224 * 224);
		memcpy(combined_planes + (224 * 224 * 2), blue.data, 224 * 224);
		float real_thing[224 * 224 * 3] = {0};
		for (size_t i = 0; i < 224 * 224 * 3; i++)
		{
			real_thing[i] = (float)combined_planes[i] / 255.0;
		}
		// Hope it was worth it...

		std::vector<const char *> inputNames{inputName};

		std::vector<Ort::Value> inputTensors;

		inputTensors.push_back(Ort::Value::CreateTensor<float>(
				memoryInfo, real_thing, inputTensorSize, inputDims.data(),
				inputDims.size()));

		std::vector<Ort::Value> out = session->Run(Ort::RunOptions{nullptr},
																							 inputNames.data(), inputTensors.data(), 1, outputNames.data(), outputNames.size());
		float *landmarks = out[0].GetTensorMutableData<float>();
		float *leftq = out[1].GetTensorMutableData<float>();
		float *rightq = out[1].GetTensorMutableData<float>();
		printf("%f %f\n", *leftq, *rightq);
		int stride = 3;
		for (size_t i = 0; i < 21; i++)
		{
			int rt = i * stride;
			float x = landmarks[rt];
			float y = landmarks[rt+1];
			float z = landmarks[rt+2];
			
			printf("%f %f %f ", x, y, z);
			cv::circle(img,{(int)x,(int)y},max(z+30,5),{0,0,255});
			// printf("rt is %d\n", rt);
			// float cX = classes[rt];
			// float cY = classes[rt + 1];
			// float w = classes[rt + 2];
			// float h = classes[rt + 3];
			// float confidence = classes[rt + 4];
			// float unknown = classes[rt + 5];
			// if (left < 200.0 && right > 200.0) {
			// printf("left %f right %f cX %f cY %f %f %f\n", left, right, cX, cY, w,h);

			// }

			// if (confidence > 0.6 || unknown < 0.9)
			// {
			// 	printf("cX %f cY %f w %f h %f confidence %f unknown %f \n", cX, cY, w, h, confidence, unknown);
			// 	drawBB(cX, cY, w, h, img);
			// }
		}
		printf("\n");
		// // return 0;
		cv::imshow("coolo", img);
		while (true){
		cv::waitKey(0);}
	// }

	// std::cout << "\n\n\n";
	// for (size_t i = 0; i < 40; i++) {
	// 	std::cout << thing1[i] << " ";
	// }
}
