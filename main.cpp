#include <onnxruntime/core/providers/cpu/cpu_provider_factory.h>
#include <onnxruntime/core/session/onnxruntime_cxx_api.h>
#include <opencv2/opencv.hpp>
#include <exception>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <cmath>
#include <sstream>

#include "nms.hpp"

Ort::Env &getEnv(void)
{
	static Ort::Env env(OrtLoggingLevel::ORT_LOGGING_LEVEL_WARNING,
											"mosesNamespace");

	return env;
}

void drawBB(float cX, float cY, float w, float h, cv::Mat &img)
{
	// cX*=640.0;
	// cY*=640.0;
	// w*=640.0;
	// h*=640.0;
	cv::circle(img, {(int)cX, (int)cY}, (int)(w / 10), {0, 0, 255});

	cv::Rect rect((int)cX - w / 2, (int)cY - h / 2, (int)w, (int)h);
	cv::rectangle(img, rect, {0, 0, 255});
}



struct hand_detector_t {
	Ort::Session* session;
	Ort::MemoryInfo* memoryInfo;
	std::vector<int64_t> inputDims;
	const char* inputName;

	std::vector<const char *> outputNames;

};

struct hand_detector_t init_hand_detector(){
	GraphOptimizationLevel onnx_optim = GraphOptimizationLevel::ORT_ENABLE_ALL;
	Ort::SessionOptions sessionOptions;
	sessionOptions.SetGraphOptimizationLevel(onnx_optim);
	char *modelLocation = "best.onnx";
	Ort::Session *session = new Ort::Session(getEnv(), modelLocation, sessionOptions);
	size_t numInputNodes = session->GetInputCount();
	std::cout << "Number of Input Nodes: " << numInputNodes << std::endl;

	Ort::TypeInfo inputTypeInfo = session->GetInputTypeInfo(0);
	// Ort::TensorTypeAndShapeInfo inputTASI = inputTypeInfo.GetTensorTypeAndShapeInfo();
	// int64_t cool[10] = {0};
	// inputTASI.GetDimensions(cool, 10);
	// for (size_t i = 0; i < 10; i++) {
	// 	printf("dim %d\n", cool[i]);
	// }

	std::vector<int64_t> inputDims = inputTypeInfo.GetTensorTypeAndShapeInfo().GetShape();
	size_t num_dims = inputDims.size();
	assert(num_dims == 4); // That's what the hand tracking model wants right now; if it's not four we have the wrong guy
	std::cout << "number of dimensions is " << inputDims.size() << "\n";
	for (size_t i = 0; i < num_dims; i++)
	{
		std::cout << "dimension " << i << " is " << inputDims[i] << "\n";
	}

	std::vector<const char *> outputNames;
	Ort::AllocatorWithDefaultOptions allocator;
	char *inputName = session->GetInputName(0, allocator);
	printf("Input name: %s\n", inputName);

	for (size_t i = 0; i < session->GetOutputCount(); ++i)
	{
		auto output_name = session->GetOutputName(i, allocator);
		std::cout << "output " << i << " is " << output_name << std::endl;
		// outputNames.push_back (output_name);
		auto type_info = session->GetOutputTypeInfo(i);
		auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
		std::vector<int64_t> outputDims = tensor_info.GetShape();
		for (size_t i = 0; i < outputDims.size(); i++)
		{
			std::cout << "   dimension " << i << " is " << outputDims[i] << "\n";
		}
		// std::cout << "shape is " << tensor_info.GetShape();
	}
	outputNames.push_back("915");
	outputNames.push_back("751");

	Ort::MemoryInfo memoryInfo = Ort::MemoryInfo::CreateCpu(
			OrtAllocatorType::OrtArenaAllocator, OrtMemType::OrtMemTypeDefault);
	struct hand_detector_t hd = {session};
	hd.memoryInfo = &memoryInfo;
	hd.inputDims = inputDims;
	hd.inputName = inputName;
	hd.outputNames = outputNames;
	return hd;
	// hd.memoryInfo = &memoryInfo;
	// hd.session = session;
}
// 	hd->onnx_optim = GraphOptimizationLevel::ORT_ENABLE_ALL;
// 	Ort::SessionOptions sessionOptions;
// 	sessionOptions.SetGraphOptimizationLevel(hd->onnx_optim);
// 	char *modelLocation = "best.onnx";
// 	hd->session = Ort::Session(getEnv(), modelLocation, sessionOptions);
// 	size_t numInputNodes = hd->session.GetInputCount();
// 	std::cout << "Number of Input Nodes: " << numInputNodes << std::endl;

// 	Ort::TypeInfo inputTypeInfo = hd->session.GetInputTypeInfo(0);
// 	// Ort::TensorTypeAndShapeInfo inputTASI = inputTypeInfo.GetTensorTypeAndShapeInfo();
// 	// int64_t cool[10] = {0};
// 	// inputTASI.GetDimensions(cool, 10);
// 	// for (size_t i = 0; i < 10; i++) {
// 	// 	printf("dim %d\n", cool[i]);
// 	// }

// 	std::vector<int64_t> inputDims = inputTypeInfo.GetTensorTypeAndShapeInfo().GetShape();
// 	size_t num_dims = inputDims.size();
// 	assert(num_dims == 4); // That's what the hand tracking model wants right now; if it's not four we have the wrong guy
// 	std::cout << "number of dimensions is " << inputDims.size() << "\n";
// 	for (size_t i = 0; i < num_dims; i++)
// 	{
// 		std::cout << "dimension " << i << " is " << inputDims[i] << "\n";
// 	}

// 	std::vector<const char *> outputNames;
// 	Ort::AllocatorWithDefaultOptions allocator;
// 	char *inputName = hd->session.GetInputName(0, allocator);
// 	printf("Input name: %s\n", inputName);

// 	for (size_t i = 0; i < hd->session.GetOutputCount(); ++i)
// 	{
// 		auto output_name = hd->session.GetOutputName(i, allocator);
// 		std::cout << "output " << i << " is " << output_name << std::endl;
// 		// outputNames.push_back (output_name);
// 		auto type_info = hd->session.GetOutputTypeInfo(i);
// 		auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
// 		std::vector<int64_t> outputDims = tensor_info.GetShape();
// 		for (size_t i = 0; i < outputDims.size(); i++)
// 		{
// 			std::cout << "   dimension " << i << " is " << outputDims[i] << "\n";
// 		}
// 		// std::cout << "shape is " << tensor_info.GetShape();
// 	}
// 	outputNames.push_back("915");
// 	outputNames.push_back("751");

// 	hd->memoryInfo = Ort::MemoryInfo::CreateCpu(
// 			OrtAllocatorType::OrtArenaAllocator, OrtMemType::OrtMemTypeDefault);
// }

int main(int argc, char **argv)
{

	struct hand_detector_t hd = init_hand_detector();
	const size_t inputTensorSize = 640 * 640 * 3; // mostly from Netron and other testing
	std::vector<cv::Mat> planes;

	// Load image

	// cv::Mat img = cv::imread("righthand.png");
	cv::VideoCapture capture;
	capture.open(2);
	printf("cool %d\n", capture.isOpened());
	// ASSUMES the camera is 1280x960

	cv::Mat raw_input;
	while (true)
	{
		while (true)
		{
			bool ok = capture.read(raw_input);
			if (ok)
				break;
		}

		cv::resize(raw_input, raw_input, {640, 480});

		// std::cout << "rawcontinuous? " << raw_input.isContinuous() << "\n";

		cv::Mat img = cv::Mat::zeros(640, 640, CV_8UC3);

		// img.at<uint8_t>(640,0,0)
		memcpy(img.data + (640 * 80 * 3), raw_input.data, 640 * 480 * 3);
		// cv::imshow("coolo",img);
		// cv::waitKey(0);

		// std::cout << "continuous? " << img.isContinuous() << "\n";
		assert(img.isContinuous());

		// Make image planar instead of interleaved
		cv::split(img, planes);
		cv::Mat red = planes[2];
		cv::Mat green = planes[1];
		cv::Mat blue = planes[0];
		uint8_t combined_planes[640 * 640 * 3] = {0};
		memcpy(combined_planes, red.data, 640 * 640);
		memcpy(combined_planes + (640 * 640), green.data, 640 * 640);
		memcpy(combined_planes + (640 * 640 * 2), blue.data, 640 * 640);
		float real_thing[640 * 640 * 3] = {0};
		for (size_t i = 0; i < 640 * 640 * 3; i++)
		{
			real_thing[i] = (float)combined_planes[i] / 255.0;
		}
		// Hope it was worth it...

		std::vector<const char *> inputNames{hd.inputName};

		std::vector<Ort::Value> inputTensors;
		printf("heraefsdf asf%p %p %p \n", hd.memoryInfo, &hd.inputDims, hd.inputDims.data());

		inputTensors.push_back(Ort::Value::CreateTensor<float>(
				*hd.memoryInfo, real_thing, inputTensorSize, hd.inputDims.data(),
				hd.inputDims.size()));

		std::vector<Ort::Value> out = hd.session->Run(Ort::RunOptions{nullptr},
																							 inputNames.data(), inputTensors.data(), 1, hd.outputNames.data(), hd.outputNames.size());
		float *classes = out[0].GetTensorMutableData<float>();
		float *boxes = out[1].GetTensorMutableData<float>();
		int stride = 6;
		std::vector<detection> detections;
		int count = 0;
		for (size_t i = 0; i < 25200; i++)
		{
			int rt = i * stride;
			// printf("rt is %d\n", rt);
			float x = classes[rt];
			float y = classes[rt + 1];
			float w = classes[rt + 2];
			float h = classes[rt + 3];
			float confidence = classes[rt + 4];
			float unknown = classes[rt + 5];
			// if (left < 200.0 && right > 200.0) {
			// printf("left %f right %f cX %f cY %f %f %f\n", left, right, cX, cY, w,h);

			// }

			if (confidence > 0.6)
			{
				detection det;
				det.bbox.w = w;
				det.bbox.h = h;
				det.bbox.x = x;
				det.bbox.y = y;
				det.conf = confidence;
				det.class_id = 1;
				det.prob = confidence;

				detections.push_back(det);
				count++;
			}
		}
		FilterBoxesNMS(detections, count, 0.7);
		int blow = 0;
		int border = 50;

		cv::copyMakeBorder(img, img, border, border, border, border, 0, {0, 0, 0});

		for (detection &det : detections)
		{

			if (det.prob > 0.001)
			{
				float x = det.bbox.x;
				float y = det.bbox.y;
				float w = det.bbox.w;
				float h = det.bbox.h;
				int square = max(w, h);

				float left = max(0, border + x - square / 2);
				float right = min(640+border*2, border + x + square / 2);

				float top = max(0, border + y - square / 2);
				float bottom = min(640+border*2, border + y + square / 2);

				printf("%f %f %f %f\n", left, right, top, bottom);
				cv::Mat subImage = img(cv::Rect(cv::Point2f{left,top},cv::Point2f{right,bottom}));
				cv::resize(subImage,subImage,{224,224});
				char name[6];
				snprintf(name,6,"cool%i",blow);
				cv::imshow(name,subImage);
				drawBB(det.bbox.x + 50, det.bbox.y + 50, det.bbox.w, det.bbox.h, img);
				blow++;
			}
		}
		// printf("\n");

		// return 0;
		cv::imshow("coolo", img);
		cv::waitKey(1);
	}
}
